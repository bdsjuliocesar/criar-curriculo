

class Register extends React.Component{
    
      render(){
        return(
          <form ref={this.formRef} noValidate>
            <div>
    
            <input type="button" value="Criar Currículo"/>
    
          <h1>Cadastro</h1>
    
                Nome da Pessoa: <br />
                <input type="text" name="userName" value={this.state.userName} ref={this.nameRef}
                    required onChange={event => this.handleInput(event, this.nameRef)}/>
    
    
                  {
                    this.state.validation.userName &&
                    !this.state.validation.userName.valid &&
                    <div className="error-message">Nome é obrigatório</div>
                  }
    
                <br />
                E-mail da Pessoa: <br/>
                <input type="email" name="userEmail" value={this.state.userEmail} ref={this.emailRef}
                    required onChange={event => this.handleInput(event, this.emailRef)}/>
    
    
                  {
                    this.state.validation.userEmail &&
                    !this.state.validation.userEmail.valid &&
                    <div className="error-message">E-mail é obrigatório</div>
                  }
    
                  {
                    
                      this.state.validation.userEmail &&
                      !this.state.validation.userEmail.valueMissing &&
                      !this.state.validation.userEmail.valid &&
                      <div className="error-message">E-mail inválido</div>
                  
                  }
    
                <br />
    
                Telefone da Pessoa: <br/>
                <input type="text" name="userPhone" value={this.state.userPhone} ref={this.phoneRef}
                    required onChange={event => this.handleInput(event, this.phoneRef)}/>
    
    
                  {
                    this.state.validation.userPhone &&
                    !this.state.validation.userPhone.valid &&
                    <div className="error-message">Telefone é obrigatório</div>
                  }
                <br />
                
                Cargo da Pessoa: <br/>
                <input type="text" name="userRole" value={this.state.userRole} ref={this.roleRef}
                    required onChange={event => this.handleInput(event, this.roleRef)}/>
    
    
                  {
                    this.state.validation.userRole &&
                    !this.state.validation.userRole.valid &&
                    <div className="error-message">Cargo é obrigatório</div>
                  }
                <br />
    
                Histórico da Pessoa: <br/>
                <textarea cols="40" rows="5"></textarea>
    
    
                 
                <br />
    
                <input type="button" value="Salvar" disabled={!this.state.validation.form}/>
    
            </div>
    
    
          </form>
        )
      }
}

export default Register;