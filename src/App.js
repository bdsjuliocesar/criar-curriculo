import { directive } from "@babel/types";
import React from "react";
import "./App.css"
import returnPage from "./routes"

//Trabalho Programação IV - Julio And Eric
//Não conseguimos trocar de tela

class App extends React.Component {


  constructor(){
    super()
    this.state = {
      viewType: 0,
      userName: '',
      userEmail: '',
      userPhone: '',
      userRole: '',
      userExperience: '',
      validation: {
        form: '',
        userName: '',
        userEmail: '',
        userPhone: '',
        userRole: '',
        userExperience: '',
      },
      formValid: null,
      userNameValid: null,
      userEmailValid: null,
      userPhoneValid: null,
      userRoleValid: null,
      userExperienceValid: null
    }

    this.formRef = React.createRef()
    this.nameRef = React.createRef()
    this.emailRef = React.createRef()
    this.phoneRef = React.createRef()
    this.roleRef = React.createRef()
    this.experienceRef = React.createRef()
  }

  handleInput(event, fieldRef) {
    let fieldName = event.target.name

    let currentValidation = this.state.validation
    currentValidation.form = this.formRef.current.checkValidity()
    currentValidation[fieldName] = fieldRef.current.validity

    this.setState({
        [fieldName]: event.target.value,
        validation: currentValidation
    })
}


  render(){

    
    
    return(  
        <button className="button-create-curriculum" onClick="returnPage()"
        type="button">Criar Curriculo</button>       
               

    )
  }

 

  


  
}
export default App;
